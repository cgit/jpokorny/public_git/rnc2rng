#!/bin/bash
# Copyright 2013 Red Hat, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# - Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
# - Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# - Neither the name of the Red Hat, Inc. nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

# TODO: the output will never be 1:1 to that from trang, so a subset of
#       test suite has to be run against manually checked outputs
set -u

SCRIPT=./rnc2rng
DIFF="diff -q"
KNOWNTOFAIL="xsdtest.rng"  # colon-separated
KNOWNTOFAIL=":${KNOWNTOFAIL}:"

run() {
	for rnc in *.rnc; do
		local rng="$(echo "${rnc}" | sed 's|rnc$|rng|')"
		echo ":${KNOWNTOFAIL}:" | grep -Fq "${rng}" && continue
		${SCRIPT} "${rnc}" > "${rng}.testrun"
		${DIFF} ${rng}{.expected,.testrun}
		[ $? -eq 0 ] && echo "${rnc} OK" || echo "${rnc} FAIL"
	done
}

clean() {
	rm -vf "*.testrun"
}

[ $# -eq 0 ] && run || $1
